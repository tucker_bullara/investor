﻿using System.Linq;
using Investor.Web.Data.Data;
using Investor.Web.Data.Models;

namespace Investor.Web.Services
{
    public static class AccountService
    {
        public static void UpdateAccount(Account account, InvestorContext dbContext)
        {
            dbContext.Update(account);
            dbContext.SaveChanges();
        }

        public static bool IsEmailInUse(string email, InvestorContext dbContext)
        {
            return dbContext.Accounts.Any(a => a.Email == email);
        }
    }
}