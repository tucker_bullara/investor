﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Investor.Web.Data.Data;
using Investor.Web.Data.Models;
using Investor.Web.Pages;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Investor.Web.Services
{
    public class SignInService
    {
        private readonly ILogger<SignInService> _logger;
        private readonly InvestorContext _dbContext;

        public SignInService(ILogger<SignInService> logger, InvestorContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public AccountStatus ValidateAccount(string email, string password)
        {
            var account = _dbContext.Accounts.FirstOrDefault(e => e.Email == email);
            
            if (account == null)
            {
                _logger.LogInformation("No account with this email: " + email);
                return AccountStatus.Unauthenticated;
            }

            var hashedPassword = PasswordHasher.HashPassword(password, account.PasswordSalt);

            return hashedPassword.Item1 == account.PasswordHash ? AccountStatus.Ok : AccountStatus.Unauthenticated;
        }

        public bool SignIn(string email, bool rememberMe, HttpContext httpContext)
        {
            var account = _dbContext.Accounts.FirstOrDefault(e => e.Email == email);
            if (account == null)
            {
                var msg = $"email: {email} does not exist";
                throw new ArgumentException(msg, nameof(email));
            }

            var profile = _dbContext.Profiles.FirstOrDefault(p => p.AccountID == account.ID);

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, account.Email),
                new Claim(ClaimTypes.NameIdentifier, account.ID.ToString()),
                new Claim(ClaimTypes.GivenName, profile?.FirstName),
                new Claim(ClaimTypes.Surname, profile?.LastName),
                new Claim(ClaimTypes.Name, profile?.PreferredName)
            };

            if (profile?.Birthday != null)
            {
                claims.Add(new Claim(ClaimTypes.DateOfBirth, profile.Birthday));
            }

            if (!string.IsNullOrWhiteSpace(profile?.Phone))
            {
                claims.Add(new Claim(ClaimTypes.OtherPhone, profile.Phone));
            }
            
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
 
            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = rememberMe ? DateTimeOffset.UtcNow.AddMonths(1) : DateTimeOffset.UtcNow.AddDays(1),
                IsPersistent = true,
                IssuedUtc = DateTime.UtcNow
            };

            httpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);

            account.LastLoginAt = DateTime.UtcNow;
            AccountService.UpdateAccount(account, _dbContext);
            
            // success
            return true;
        }
    }
}