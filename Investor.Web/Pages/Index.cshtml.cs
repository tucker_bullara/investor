﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Investor.Web.Data.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Investor.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly InvestorContext _dbContext;

        public IndexModel(ILogger<IndexModel> logger, InvestorContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public void OnGet()
        {
            //Quick test
            var result = _dbContext.Accounts.ToList();
            Console.WriteLine(result.Count);
        }
    }
}