﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Investor.Web.Data.Data;
using Investor.Web.Data.Models;
using Investor.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Investor.Web.Pages.Account
{
    // Register Class
    public class Register : PageModel
    {
        private readonly InvestorContext _dbContext;
        private readonly ILogger<Register> _logger;
        public Register(InvestorContext dbContext, ILogger<Register> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }
        
        [BindProperty] public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        // Class to create Input Model 
        public class InputModel {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.",
                MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            [Display(Name = "Preferred Name")]
            public string PreferredName { get; set; }

            [Required]
            [Display(Name = "Date Of Birth")]
            [DataType(DataType.Date)]
            public DateTime DateOfBirth { get; set; }
        }
        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public IActionResult OnPost(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");
            if (!ModelState.IsValid) return Page();

            if (AccountService.IsEmailInUse(Input.Email, _dbContext))
            {
                _logger.LogWarning($"An account with {Input.Email} already exist");
                ModelState.AddModelError("All", "Email already in use. ");
                return Page();
            }

            var (passwordHash, passwordSalt) = PasswordHasher.HashPassword(Input.Password);
            var accountToAdd = new Data.Models.Account
            {
                Email = Input.Email,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                CreatedAt = DateTime.UtcNow,
                Status = (int) AccountStatus.PendingVerification,
                UserRole = 3
            };
            
            _logger.LogTrace($"Attempting to create account for {accountToAdd.Email}");
            _dbContext.Accounts.Add(accountToAdd);
            _dbContext.SaveChanges();
            _logger.LogInformation($"Created account {accountToAdd.Email}");
            
            var account = _dbContext.Accounts.First(a => a.ID == accountToAdd.ID);
            
            // TODO: populate other fields such as phone and birthday
            var profileToAdd = new Profile
            {
                AccountID = account.ID,
                FirstName = Input.FirstName,
                LastName = Input.LastName,
                PreferredName = Input.PreferredName,
            };
            
            _logger.LogTrace($"Attempting to create profile for Account {account.ID}");
            _dbContext.Profiles.Add(profileToAdd);
            _dbContext.SaveChanges();
            _logger.LogInformation("Created profile.");

            return RedirectToPage("Login");
        }
    }
}