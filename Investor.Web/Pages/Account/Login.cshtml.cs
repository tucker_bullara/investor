﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using Investor.Web.Data.Models;
using Investor.Web.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Investor.Web.Pages.Account
{
    public class Login : PageModel
    {
        private readonly SignInService _signInService;

        public Login(SignInService signInService)
        {
            _signInService = signInService;
        }
        
        [BindProperty] 
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }
        
        [TempData] 
        public string ErrorMessage { get; set; }
        
        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }
            
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }
            
            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
        }
        public void OnGet(string returnUrl = null)
        {
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl ??= Url.Content("~/");

            HttpContext.SignOutAsync();
            ReturnUrl = returnUrl;
        }

        public IActionResult OnPost(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");

            if (ModelState.IsValid)
            {
                switch (_signInService.ValidateAccount(Input.Email, Input.Password))
                {
                    case AccountStatus.Banned:
                        ModelState.AddModelError(string.Empty, "Your account has been banned LOL");
                        return Page();
                    case AccountStatus.Disabled:
                        ModelState.AddModelError(string.Empty, "Your account has been disabled.");
                        return Page();
                    case AccountStatus.Unauthenticated:
                        ModelState.AddModelError(string.Empty, "Invalid credentials.");
                        return Page();
                    case AccountStatus.LockedOut:
                        ModelState.AddModelError(string.Empty, "Account locked.");
                        return Page();
                    case AccountStatus.PendingVerification:
                        ModelState.AddModelError(string.Empty, "Your account is pending verification.");
                        return Page();
                    case AccountStatus.Ok:
                        _signInService.SignIn(Input.Email, Input.RememberMe, HttpContext);
                        return RedirectToPage("Success");
                }
            }
            
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError(string.Empty, "An unexpected error occured.");
            return Page();
        }

        public async Task<IActionResult> OnGetLogout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return LocalRedirect(Url.Content("~/"));
        }
    }
}