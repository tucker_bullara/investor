using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection.Metadata;
using FusionCharts.DataEngine;
using FusionCharts.Visualization;
using Investor.Web.Data.Data;
using Investor.Web.Data.Models;
using Investor.Web.Pages.Admin.Account;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Investor.Web.Pages.Users
{
    
    
    public class PortfolioPage : PageModel
    {
        private readonly InvestorContext _dbContext;
        public IEnumerable<Data.Models.Stock> PendingStocks;

        public PortfolioPage(InvestorContext dbContext)
        {
            _dbContext = dbContext;
        }
        public string ChartJson { get; internal set; }

        public void OnGet()
        {
            /** Possible Ticker Values:
             *    1. Apple = "aapl"
             *    2. Dropbox = "dbx"
             *    3. Alphabet inc = "googl"
             *    4. Sony = "sne"
             */
            
            /**
             *  What to do: 1. trace back all possible stocks from DB instead of using strArray[1] != "none"
             *              2. Fix days on the chart
             *              3. Create a default blank chart when no stock or day is selected
             */

            string[] possDays = {"10", "30", "60"}; //Keeps track of all possible days for time frame of chart
            string[] possStocks = { }; //Keeps track of all possible stocks in our database
            
            if (!string.IsNullOrEmpty(HttpContext.Request.QueryString.Value))
            {
                string queryTest = HttpContext.Request.QueryString.Value;
                string[] strArray = queryTest.Split(new char[] {'=', '&'});
                if (strArray[1] != "none" && Int32.Parse(strArray[3]) != 0)
                {
                    var Stock = new StockData(strArray[1]); // Sets current stock user is looking at.
                    var data = Stock.GetRange(Int32.Parse(strArray[3]));

                    DataTable chartData = new DataTable();

                    chartData.Columns.Add("Day", typeof(System.String));
                    chartData.Columns.Add("Price", typeof(System.Double));

                    //Console.WriteLine("Count: " + data.Count);
                    int addedDays = 0;
                    DateTime date = new DateTime();
                    for (int i = 0; i < data["open"].Count; i++)
                    {
                        date = DateTime.Today.AddDays(-i - addedDays);
                        if (date.DayOfWeek.ToString() == "Monday")
                        {
                            addedDays += 2;
                        } else if (date.DayOfWeek.ToString() == "Sunday")
                        {
                            addedDays += 1;
                        }

                        string[] parsedDateArr = date.ToShortDateString().Split("/");
                        string newDate = parsedDateArr[0] + "/" + parsedDateArr[1];
                        chartData.Rows.Add(newDate, data["open"][i]);
                    }
 
                    StaticSource source = new StaticSource(chartData);
                    DataModel model = new DataModel();
                    model.DataSources.Add(source);
                    Charts.LineChart
                        line = new Charts.LineChart(
                            "Stock_Data"); // Note: "Stock Data" had to be changed to "Stock_Data"

                    line.ThemeName = FusionChartsTheme.ThemeName.FUSION;
                    line.Width.Percentage(100);
                    line.Height.Pixel(550);

                    line.Data.Source = model;

                    line.Caption.Text = strArray[1].ToUpper();
                    line.Caption.Bold = true;

                    //line.SubCaption.Text = "Last Week";
                    line.XAxis.Text = "Dates (" + strArray[3] + " Days)";
                    line.YAxis.Text = "Price";

                    line.Legend.Show = false;

                    ChartJson = line.Render();
                }
                else //The default chart with no inputs from the user
                {
                    DataTable chartData = new DataTable();

                    chartData.Columns.Add("Day", typeof(System.String));
                    chartData.Columns.Add("Price", typeof(System.Double));
                    chartData.Rows.Add("", 00);
                    
                    StaticSource source = new StaticSource(chartData);
                    DataModel model = new DataModel();
                    model.DataSources.Add(source);
                    Charts.LineChart
                        line = new Charts.LineChart(
                            " "); // Note: "Stock Data" had to be changed to "Stock_Data"
                    line.ThemeName = FusionChartsTheme.ThemeName.FUSION;
                    line.Width.Percentage(100);
                    line.Height.Pixel(550);
                    

                    line.Data.Source = model;

                    line.Caption.Text = "<STOCK>";
                    line.Caption.Bold = true;
                    
                    line.XAxis.Text = "Dates";
                    line.YAxis.Text = "Price";

                    line.Legend.Show = false;

                    ChartJson = line.Render();
                }
            }
        }
        
    }
}