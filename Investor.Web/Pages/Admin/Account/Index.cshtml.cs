﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Investor.Web.Data.Data;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Investor.Web.Pages.Admin.Account
{
    public class Index : PageModel
    {
        private readonly InvestorContext _dbContext;

        public Index(InvestorContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Data.Models.Account> Accounts;
        
        public void OnGet()
        {
            Accounts = _dbContext.Accounts.ToList();
        }
    }
}