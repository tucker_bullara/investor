﻿using System.Collections.Generic;
using System.Linq;
using Investor.Web.Data.Data;
using Investor.Web.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Investor.Web.Pages.Admin.Account
{
    public class Pending : PageModel
    {
        private readonly InvestorContext _dbContext;
        public IEnumerable<Data.Models.Account> PendingAccounts;

        public Pending(InvestorContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public void OnGet()
        {
            PendingAccounts = _dbContext.Accounts.Where(p => p.Status == (int) AccountStatus.PendingVerification);
        }

        public IActionResult OnGetUpdateStatus(int id, bool approve)
        {
            var account = _dbContext.Accounts.FirstOrDefault(f => f.ID == id);
            if (account == null) return NotFound();

            account.Status = approve ? (int)AccountStatus.Ok : (int)AccountStatus.Disabled;
            _dbContext.Accounts.Update(account);
            _dbContext.SaveChanges();
            return RedirectToPage();
        }
    }
}