﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Investor.Web.Data.Data;
using Investor.Web.Data.Models;
using Investor.Web.Pages.Account;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Investor.Web.Pages.Admin.Account
{
    public class Edit : PageModel
    {
        private readonly InvestorContext _dbContext;
        
        public Data.Models.Account Account { get; set; }
        public Data.Models.Profile Profile { get; set; }
        
        [BindProperty]
        public InputModel Input { get; set; }

        public Edit(InvestorContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public class InputModel {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }
            
            [Required]
            [Display(Name = "Account Status")]
            [EnumDataType(typeof(AccountStatus))]
            public AccountStatus Status { get; set; }
            
            [Required]
            [Display(Name = "User Role")]
            [EnumDataType(typeof(UserRoles))]
            public UserRoles UserRole { get; set; }
            
            [Required]
            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            [Display(Name = "PreferredName")]
            public string PreferredName { get; set; }
            
            [Display(Name = "Phone Number")]
            [DataType(DataType.PhoneNumber)]
            public string Phone { get; set; }

            [Display(Name = "Date Of Birth")]
            [DataType(DataType.Date)]
            public DateTime? DateOfBirth { get; set; }
        }
        
        public IActionResult OnGet(int id)
        {
            Account = _dbContext.Accounts.FirstOrDefault(f => f.ID == id);
            Profile = _dbContext.Profiles.FirstOrDefault(f => f.AccountID == id);
            if (Account == null || Profile == null) return NotFound();

            Input = new InputModel()
            {
                FirstName = Profile.FirstName,
                LastName = Profile.LastName,
                PreferredName = Profile.PreferredName,
                Phone = Profile.Phone,
                Email = Account.Email,
                UserRole = (UserRoles)Account.UserRole,
                Status = (AccountStatus)Account.Status
            };
            return Page();
        }
        
        public IActionResult OnPostGeneral(int id)
        {
            Account = _dbContext.Accounts.FirstOrDefault(f => f.ID == id);
            Profile = _dbContext.Profiles.FirstOrDefault(f => f.AccountID == id);
            if (Account == null || Profile == null) return NotFound();

            Account.Email = Input.Email;
            Account.Status = (int)Input.Status;
            Account.LastModifiedAt = DateTime.UtcNow;
            Account.UserRole = (int)Input.UserRole;
            
            Profile.Phone = Input.Phone;
            Profile.FirstName = Input.FirstName;
            Profile.LastName = Input.LastName;
            Profile.PreferredName = Input.PreferredName;

            _dbContext.Update(Account);
            _dbContext.Update(Profile);
            _dbContext.SaveChanges();
            
            return RedirectToPage();
        }
    }
}