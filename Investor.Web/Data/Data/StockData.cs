using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace Investor.Web.Data.Data
{
    public class StockData
    {
        private const string SandboxBaseUrl = "https://sandbox.iexapis.com/stable/stock/";
        private const string BaseUrl = "https://cloud.iexapis.com/stable/stock/";
        private const string SandboxToken = "Tpk_de94e88066424a5f8758d1a1297b9c99";
        private const string Token = "pk_4dc1c4bd9bb44b3ea78cd7b57cfb9195";
        private readonly string _ticker;
        private readonly bool _sandbox;
        
        public StockData(string ticker, bool sandbox = true)
        {
            this._ticker = ticker;
            this._sandbox = sandbox;
        }
        
        public Double GetOpen(string yyyymmdd)
        {
            return double.Parse(GetDate(yyyymmdd)["open"]);
        }
        public Double GetHigh(string yyyymmdd)
        {
            return double.Parse(GetDate(yyyymmdd)["high"]);
        }
        public Double GetLow(string yyyymmdd)
        {
            return double.Parse(GetDate(yyyymmdd)["low"]);
        }
        public Double GetClose(string yyyymmdd)
        {
            return double.Parse(GetDate(yyyymmdd)["close"]);
        }

        public Dictionary<string, string> GetDate(string yyyymmdd)
        {
            string url = (_sandbox ? SandboxBaseUrl : BaseUrl) + _ticker + "/chart/date/" + yyyymmdd + "/?chartByDay=true&token=" + (_sandbox ? SandboxToken : Token);
            var requestObj = (HttpWebRequest)WebRequest.Create(url);
            requestObj.Method = "GET";
            var responseObj = (HttpWebResponse)requestObj.GetResponse();

            string result = null;
            using (var stream = responseObj.GetResponseStream())
            {
                if (stream != null)
                {
                    var sr = new StreamReader(stream);
                    result = sr.ReadToEnd();
                    sr.Close();
                }
                
            }
            var obj = JsonConvert.DeserializeObject<List<Dictionary<string,string>>>(result);

            return obj.Count != 0 ? obj[0] : null;
        }

        // returns a dictionary with (open, close, high, low) as key and (int price) as value starting n days before to current date. 
        public Dictionary<string, List<double>> GetRange(int n)
        {
            var date = DateTime.Now;
            List<double> open = new List<double>();
            List<double> high = new List<double>();
            List<double> low = new List<double>();
            List<double> close = new List<double>();
            Dictionary<string, string> getDateDict = new Dictionary<string, string>();
            for (var i = 0; i < n; i++)
            {
                if (date.AddDays(-i).DayOfWeek.ToString() == "Saturday" ||
                    date.AddDays(-i).DayOfWeek.ToString() == "Sunday")
                {
                    Console.WriteLine(date.AddDays(-i).DayOfWeek.ToString());
                }
                else
                {
                    getDateDict = GetDate(date.AddDays(-i).ToString("yyyyMMdd"));
                    if (getDateDict != null)
                    {
                        try
                        {
                            open.Add(double.Parse(getDateDict["open"]));
                            high.Add(double.Parse(getDateDict["high"]));
                            low.Add(double.Parse(getDateDict["low"]));
                            close.Add(double.Parse(getDateDict["close"]));
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Unable to Parse '{i}'");
                        }
                    }
                }
            }

            var dataDict = new Dictionary<string, List<double>>()
            {
                {"open", open},
                {"high", high},
                {"low", low},
                {"close", close}
            };
            
            return dataDict;
        }
    }
}