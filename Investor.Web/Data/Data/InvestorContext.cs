﻿using Investor.Web.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Investor.Web.Data.Data
{
    public class InvestorContext : DbContext
    {
        public InvestorContext(DbContextOptions<InvestorContext> options) : base(options)
        {
        }
        
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Portfolio> Portfolios { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Stock> Stocks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>().ToTable("Account");
            modelBuilder.Entity<Portfolio>().ToTable("Portfolio");
            modelBuilder.Entity<Profile>().ToTable("Profile");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<Stock>().ToTable("Stock");
        }
    }
}