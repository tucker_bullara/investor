﻿namespace Investor.Web.Data.Models
{
    public class Portfolio
    {
        public int ID { get; set; }
        public int ProfileID { get; set; }
        public double AmountInvested { get; set; }
        public double AmountToInvest { get; set; }
    }
}