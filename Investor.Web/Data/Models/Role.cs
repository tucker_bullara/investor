﻿using Microsoft.EntityFrameworkCore.Design;

namespace Investor.Web.Data.Models
{
    public class Role
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}