﻿namespace Investor.Web.Data.Models
{
    public class Profile
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PreferredName { get; set; }
        public string Phone { get; set; }
        public string Birthday { get; set; }
        public int AccountID { get; set; }
    }
}