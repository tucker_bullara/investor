﻿using System;

namespace Investor.Web.Data.Models
{
    public class Account
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public int Status { get; set; }
        
        public int UserRole { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastLoginAt { get; set; }
        public DateTime? LastModifiedAt { get; set; }
    }

    public enum AccountStatus
    {
        Disabled,
        Banned,
        PendingVerification,
        LockedOut,
        Unauthenticated,
        Ok
    }

    public enum UserRoles
    {
        Admin,
        CompanyRep,
        User
    }
}