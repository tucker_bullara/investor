using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Investor.Web.Data.Data;
using Investor.Web.Data.Models;
using Investor.Web.Services;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace InvestorTests
{

    [TestFixture]
    public class RegisterTests
    {
        //TODO: Setup more unit testing

        [SetUp]
        public void Setup()
        {
        }
        
        [Test]
        public void PassingAccountInsertionTest()
        {
            
        }
        
        [Test]
        public void UpdateAccountTest1()
        {
            var current = new Account {Email = "yo@test.gov", Status = 1, UserRole = 3};
            
            // Update account
            current.Email = "test@test.gov";
            current.Status = 2;
            current.UserRole = 1;
            
            Assert.AreNotEqual(current, new Account{Email = "test@test.gov", Status = 2, UserRole = 1});
        }

        [Test]
        public void UpdateAccountTest2()
        {
            var current = new Account {Email = "yo@test.gov", Status = 1, UserRole = 3};
            Assert.AreNotEqual(current, new Account {Email = "yo@tedfst.gov", Status = 1, UserRole = 3});
        }
    }
 }