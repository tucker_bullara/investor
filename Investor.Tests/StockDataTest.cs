using Investor.Web.Data.Data;
using NUnit.Framework;

namespace InvestorTests
{
    [TestFixture]
    public class StockDataTest
    {
        StockData stk = new StockData("aapl", false);
        
        [Test]
        public void Test1()
        {
            Assert.AreEqual(268.70, stk.GetOpen("20200409"));
        }
        
        [Test]
        public void Test2()
        {
            Assert.AreEqual(270.07, stk.GetHigh("20200409"));
        }
        
        [Test]
        public void Test3()
        {
            Assert.AreEqual(264.70, stk.GetLow("20200409"));
        }
        
        [Test]
        public void Test4()
        {
            Assert.AreEqual(267.99, stk.GetClose("20200409"));
        }
    }
}